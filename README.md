# RSA Magic Public Key
_Converting between the Magic Public Key format and a RustCrypto RSA type_

```rust
use rsa_magic_public_key::*;
use rsa::{RSAPrivateKey, RSAPublicKey};

let mut rng = rand::thread_rng();
let rsa = RSAPrivateKey::new(&mut rng, 2048)?;

let magic_public_key: String = rsa.as_magic_public_key();

let res = RSAPublicKey::from_magic_public_key(&magic_public_key);

assert!(res.is_ok());
```

### Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

### License

Copyright © 2023 Riley Trautman

rsa-magic-public-key is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rsa-magic-public-key is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of rsa-magic-public-key.

You should have received a copy of the GNU General Public License along with rsa-magic-public-key. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
